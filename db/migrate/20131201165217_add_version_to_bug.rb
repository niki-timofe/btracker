class AddVersionToBug < ActiveRecord::Migration
  def change
    add_column :bugs, :version, :string
  end
end
