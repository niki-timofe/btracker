module BugsHelper
  def get_state_name(state_number)
    case state_number
      when 0 then
        'New'
      when 1 then
        'Investigating'
      when 2 then
        'Performing bug fix'
      when 3 then
        'Unconfirmed'
      when 4 then
        'Will not be fixed'
      when 5 then
        'Fixed'
      else
        'Unidentified'
    end

  end
end
