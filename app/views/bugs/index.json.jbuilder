json.array!(@bugs) do |bug|
  json.extract! bug, :description, :contact, :version, :state
  json.url bug_url(bug, format: :json)
end
