json.array!(@settings) do |setting|
  json.extract! setting, :var, :value
  json.url edit_site_setting_url(setting, format: :json)
end
