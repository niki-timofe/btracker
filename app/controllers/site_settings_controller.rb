class SiteSettingsController < ApplicationController
  before_action :set_setting, only: [:update]
  before_filter :authenticate

  def index
    # to get all items for render list
    @settings = SiteSettings.unscoped
  end

  def update
    respond_to do |format|
      SiteSettings[params[:id]] = setting_params[:value]
      logger.debug setting_params[:value]
      format.html { redirect_to site_settings_path, notice: 'Settings was successfully updated.' }
      format.json { head :no_content }
    end
  end

  private

  def set_setting

  end

  def setting_params
    params.require(:site_settings).permit(:value)
  end
end
