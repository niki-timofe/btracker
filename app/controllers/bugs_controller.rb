class BugsController < ApplicationController
  before_action :set_bug, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate, :except => :create
  before_filter :authenticate_user, :only => :create
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }


  # GET /bugs
  # GET /bugs.json
  def index
    if request.format == 'application/json'
      @bugs = Bug.order(:created_at).order(:state).limit(params[:limit]).all
    else
      @bugs = Bug.order(:created_at).order(:state).paginate(:page => params[:page], :per_page => 50)
    end
  end

  # GET /bugs/1
  # GET /bugs/1.json
  def show
  end

  # GET /bugs/new
  def new
    @bug = Bug.new
  end

  # GET /bugs/1/edit
  def edit
  end

  # POST /bugs
  # POST /bugs.json
  def create
    @bug = Bug.new(bug_params)

    respond_to do |format|
      if @bug.save
        format.html { redirect_to @bug, notice: 'Bug was successfully created.' }
        format.json { render action: 'show', status: :created, location: @bug }
      else
        format.html { render action: 'new' }
        format.json { render json: @bug.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bugs/1
  # PATCH/PUT /bugs/1.json
  def update
    respond_to do |format|
      if @bug.update(bug_params)
        format.html { redirect_to @bug, notice: 'Bug was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @bug.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bugs/1
  # DELETE /bugs/1.json
  def destroy
    @bug.destroy
    respond_to do |format|
      format.html { redirect_to bugs_url }
      format.json { head :no_content }
    end
  end

  private
  def authenticate_user
    authenticate_or_request_with_http_basic do |username, password|
      username == SiteSettings.user_name && password == SiteSettings.user_password
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_bug
    @bug = Bug.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def bug_params
    params.require(:bug).permit(:description, :contact, :version, :state)
  end

  def get_name_from_settings
    logger.debug "Name is: #{SiteSettings.admin_name}"
    logger.debug "Password is: #{SiteSettings.password}"
  end
end
